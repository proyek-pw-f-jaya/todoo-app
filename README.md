# Todoo: To-do list web wpp for Pemweb Project

# How to use

-   Clone repository **git clone https://gitlab.com/proyek-pw-f-jaya/todoo-app.git**
-   Sesuaikan informasi database pada file **.env**
-   Jalankan **composer install**
-   Jalankan **composer update**
-   Jalankan **php artisan key:generate**
-   Jalankan **php artisan migrate**
